#!/usr/bin/env python

import os
import sys
import argparse
import io
import tempfile
import lzma

def tmpfile(path):
	return tempfile.NamedTemporaryFile(
		'wb',
		suffix='.tmp',
		prefix=os.path.basename(path) + '.',
		dir=os.path.dirname(path),
		delete=False
	)

def compress(src, dst, buffer_size=io.DEFAULT_BUFFER_SIZE):
	tmp = tmpfile(dst)
	try:
		with open(src, 'rb') as fi:
			with lzma.open(
				tmp,
				'wb',
				format=lzma.FORMAT_XZ,
				check=lzma.CHECK_CRC64,
				preset=lzma.PRESET_EXTREME | 9
			) as fo:
				while True:
					buff = fi.read(buffer_size)
					if len(buff):
						fo.write(buff)
					else:
						break
				fo.close()
			fi.close()
		tmp.close()
		os.rename(tmp.name, dst)
	except:
		if not tmp.closed:
			tmp.close()
		os.remove(tmp.name)
		raise

def decompress(src, dst, buffer_size=io.DEFAULT_BUFFER_SIZE):
	tmp = tmpfile(dst)
	try:
		with lzma.open(src, 'rb') as fi:
			while True:
				buff = fi.read(buffer_size)
				if len(buff):
					tmp.write(buff)
				else:
					break
		tmp.close()
		os.rename(tmp.name, dst)
	except:
		if not tmp.closed:
			tmp.close()
		os.remove(tmp.name)
		raise

def process_file(options, src):
	fl = os.path.basename(src).lower()
	dst = None
	comp = False
	decomp = False

	if options.compress and fl.endswith('.lif'):
		dst = src + '.xz'
		comp = True
	elif options.decompress and fl.endswith('.lif.xz'):
		dst = src[:-3]
		decomp = True
	else:
		return

	if os.path.exists(dst):
		print('Existing: %s' % (dst))
		return

	if comp:
		print('Compressing: %s %s' % (src, dst))
		compress(src, dst)
	elif decomp:
		print('Decompressing: %s %s' % (src, dst))
		decompress(src, dst)

def process(options):
	for p in options.paths:
		if os.path.isdir(p):
			for root, dirs, files in os.walk(p):
				for f in files:
					process_file(options, os.path.join(root, f))
		else:
			process_file(options, p)

def main():
	parser = argparse.ArgumentParser()

	group = parser.add_mutually_exclusive_group(required=True)
	group.add_argument(
		'-c',
		'--compress',
		action='store_true',
		help='Compress any .lif files'
	)
	group.add_argument(
		'-d',
		'--decompress',
		action='store_true',
		help='Decompress any .lif.xz files'
	)

	parser.add_argument(
		'paths',
		nargs='*',
		default=['.'],
		help='Paths to search in (default to working directory)'
	)

	process(parser.parse_args())
	return 0

if __name__ == '__main__':
	sys.exit(main())
