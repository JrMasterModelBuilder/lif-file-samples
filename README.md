# LIF File Samples

LIF file format samples.


## About

This repository contains a collection of `.lif` files, useful for testing against.

All `.lif` files are compressed to `.xz` files using LZMA to reduce file size.

All `.xz` files are stored using Git Large File Storage (LFS).

A Python 3.3+ script that can decompress (and compress) them is included.

All files in this repository were compressed with this script.


## License

Everything outside of the `samples` directory is licensed under [MPL 2.0](https://www.mozilla.org/en-US/MPL/2.0/).
